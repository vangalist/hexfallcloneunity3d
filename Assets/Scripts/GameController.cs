﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameOverScreenController gameOverScreen;
    public int gridSizeX = 9;
    public int gridSizeY = 8;
    public float gridLength = 1.4f;
    public Color[] hexagonColors;
    public GridController gridController;
    public HitBoxGridController hitBoxGridController;
    public CursorController curserController;
    public PlayerController playerController;
    public static bool isGameOver = false;
    void Start()
    {
        playerController.restartGame();
        curserController.initialize(gridSizeX, gridSizeY, gridLength);
        hitBoxGridController.createGrid(gridSizeX, gridSizeY, gridLength);
        gridController.createGrid(gridSizeX, gridSizeY, gridLength, hexagonColors);
    }
    Vector3 beginTouch;
    Vector3 endTouch;

    void Update()
    {
        if (!isGameOver)
        {
            if (canGetInput())
            {
                GetInputAndAct();
            }

        }

    }
    bool  canGetInput(){
        if(HexagonController.movingHexagonNumber>0){
            return false;
        }
        return true;
    }
    void GetInputAndAct()
    {

        if (Input.GetMouseButtonDown(0))
        {
            beginTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            //time when it is pressed
        }
        else if (Input.GetMouseButtonUp(0))
        {
            endTouch = Input.mousePosition;
            Vector3 ofset = endTouch - beginTouch;
            if (ofset.magnitude < Screen.width * 0.1)
            {
                //cursor is placed
                HitBoxContoller currentHexagonHitBox = getClickedHexagonHitBox();
                if (currentHexagonHitBox != null)
                {
                    curserController.setCurserTo(currentHexagonHitBox);
                }
            }
            else
            {
                //Hexagons ins Cursor rotated
                Vector3 beginTouchWorld = Camera.main.ScreenToWorldPoint(beginTouch);
                Vector3 endTouchWorld = Camera.main.ScreenToWorldPoint(endTouch);
                beginTouchWorld.z = 0;
                endTouchWorld.z = 0;
                Vector3 rotationDirection = endTouchWorld - beginTouchWorld;
                Vector3 rotationRadious = curserController.transform.position - beginTouchWorld;
                if (Vector3.Cross(rotationDirection, rotationRadious).z > 0)
                {
                    gridController.rotateAroundCurser(false);
                }
                else
                {
                    gridController.rotateAroundCurser(true);
                }

            }
        }
    }
    HitBoxContoller getClickedHexagonHitBox()
    {
        Vector3 touchPosWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 touchPosWorld2D = new Vector2(touchPosWorld.x, touchPosWorld.y);
        RaycastHit2D hitInformation = Physics2D.Raycast(touchPosWorld2D, Camera.main.transform.forward);

        if (hitInformation.collider != null)
        {
            //We should have hit something with a 2D Physics collider!
            return hitInformation.transform.gameObject.GetComponent<HitBoxContoller>();
        }
        return null;
    }
    public void gameOver()
    {
        curserController.gameObject.SetActive(false);
        gameOverScreen.activateGameOverScreen();
        GridController.destroyAllHexagons();
        isGameOver = true;
    }
    public void restartGame()
    {
        gridController.createHexagonGrid();
        curserController.gameObject.SetActive(true);
        isGameOver = false;
        playerController.restartGame();
    }

}
