﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreenController : MonoBehaviour
{
    // Start is called before the first frame update

    public Text DestroyedHexagonsText;
    public Text scoreText;


    public void restartGame(){
        gameObject.SetActive(false);
    }
    public void activateGameOverScreen(){
        DestroyedHexagonsText.text= PlayerController.destroyedHexagonCounter.ToString();
        scoreText.text=PlayerController.score.ToString();
        gameObject.SetActive(true);
    }
}
