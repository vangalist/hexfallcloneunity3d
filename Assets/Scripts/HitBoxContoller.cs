﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxContoller : MonoBehaviour
{
    // Start is called before the first frame update
    public int x;
    public int y;
    public bool isReverse=false;

    public void initialize(int newX,int newY,bool isReversed){
        x=newX;
        y=newY;
        if(isReversed){
            gameObject.transform.localScale=new Vector3(-1,1,1);
            isReverse=true;
        }
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
     public HexagonController[] getAdjustHexagons()
    {
        HexagonController[] adjusts = new HexagonController[3];
        if (isReverse)
        {
            if (x % 2 == 0)
            {
                adjusts[0] = GridController.Hexagons[x, y];
                adjusts[1] = GridController.Hexagons[x + 1, y];
                adjusts[2] = GridController.Hexagons[x + 1, y - 1];
            }
            else
            {
                adjusts[0] = GridController.Hexagons[x, y - 1];
                adjusts[1] = GridController.Hexagons[x + 1, y];
                adjusts[2] = GridController.Hexagons[x + 1, y - 1];
            }
        }
        else
        {
            if (x % 2 == 0)
            {
                adjusts[0] = GridController.Hexagons[x, y];
                adjusts[1] = GridController.Hexagons[x, y + 1];
                adjusts[2] = GridController.Hexagons[x + 1, y];
            }
            else
            {
                adjusts[0] = GridController.Hexagons[x, y];
                adjusts[1] = GridController.Hexagons[x, y + 1];
                adjusts[2] = GridController.Hexagons[x + 1, y + 1];
            }
        }
        return adjusts;

    }

    public bool isAdjustHexagonsCombo(){
        HexagonController[] AdjustHexagons = getAdjustHexagons();
        if(AdjustHexagons[0].colorIndex==AdjustHexagons[1].colorIndex && AdjustHexagons[2].colorIndex==AdjustHexagons[1].colorIndex ){
            return true;
        }else{
            return false;
        }
    }
}
