﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject hexagon;
    public GameObject bomb;
    private int numColors;
    public static HexagonController[,] Hexagons;
    private static int gridSizeX;
    private static int gridSizeY;
    private float gridLength;
    private Color[] hexagonColors;
    public GameController gameController;
    public CursorController curserController;
    public PlayerController playerController;
    public static bool insertBomb = false;
    void Start()
    {

    }

    public void createGrid(int newGridSizeX, int newGridSizeY, float newGridLenght, Color[] newHexaColors)
    {
        gridSizeX = newGridSizeX;
        gridSizeY = newGridSizeY;
        gridLength = newGridLenght;
        hexagonColors = newHexaColors;
        HexagonController.setParameters(gridSizeX, gridSizeY, gridLength);
        Hexagons = new HexagonController[gridSizeX, gridSizeY];
        numColors = hexagonColors.Length;
        createHexagonGrid();
    }

    // Update is called once per frame

    //touch vairables

    bool isHitBoxBroken(HitBoxContoller hitBoxController)
    {
        HexagonController[] adjustHexagons = hitBoxController.getAdjustHexagons();
        if (adjustHexagons[0].color == adjustHexagons[1].color && adjustHexagons[2].color == adjustHexagons[1].color)
        {
            return true;
        }
        return false;
    }

    public void createHexagonGrid()
    {
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                int randomColorIndex = Random.Range(0, numColors);
                createHexagon(i, j, hexagonColors[randomColorIndex], randomColorIndex);
                Hexagons[i, j].setAdjustHexagons();
                int forbiddenColorIndex = Hexagons[i, j].forbidenColorIndex();
                if (forbiddenColorIndex >= 0)
                {
                    Hexagons[i, j].setColor(hexagonColors[(forbiddenColorIndex + 1) % numColors], (forbiddenColorIndex + 1) % numColors);

                }
            }
        }
    }
    private void createHexagon(int x, int y, Color color, int randomColorIndex)
    {

        GameObject newHexagon = GameObject.Instantiate(hexagon, HexagonController.calculateWorldCordinates(x, y), Quaternion.identity);
        HexagonController newHexagonController = newHexagon.GetComponent<HexagonController>();
        newHexagonController.initialize(x, y, color, randomColorIndex);
        Hexagons[x, y] = newHexagonController;
    }

    public void applyGravityOnGridSystem()
    {
        for (int i = 0; i < gridSizeX; i++)
        {

            for (int j = 0; j < gridSizeY; j++)
            {
                if (Hexagons[i, j] != null)
                {


                    Vector2Int newCordinates = Hexagons[i, j].findCordinatesAfterCombo();
                    if (newCordinates != Hexagons[i, j].gridCordinates)
                    {

                        Hexagons[newCordinates.x, newCordinates.y] = Hexagons[i, j];
                        Hexagons[i, j] = null;
                        Hexagons[newCordinates.x, newCordinates.y].setNewCordinates(newCordinates);
                    }

                }
            }

        }

    }
    public void allHexagonsStopped()
    {
        if (curserController.isRotating)
        {
            if (HitBoxGridController.getCombodHitBoxes().Count > 0)
            {
                curserController.rotationDone();
                playerController.increaseNumOfMovesBy1();
                destroyCombedHexagons();
                applyGravityOnGridSystem();
                fillEmptyCollumns();
            }
            else
            {
                rotateAroundCurser120(curserController.isDirectionRight);
            }
        }
        else
        {

            if (HitBoxGridController.getCombodHitBoxes().Count == 0)
            {
                BombController.updateAllBombCounters();
                if(!isExistAvailableMoves()){
                    gameController.gameOver();
                }
            }
            else
            {
                destroyCombedHexagons();
                applyGravityOnGridSystem();
                fillEmptyCollumns();
            }
        }
    }
    public bool isExistAvailableMoves(){
       
        //TODO

        return true;
    }
    public void fillEmptyCollumns()
    {
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                if (Hexagons[i, j] == null)
                {
                    if (insertBomb)
                    {

                        GameObject newBomb = GameObject.Instantiate(bomb, HexagonController.calculateWorldCordinates(i, gridSizeY + 4), Quaternion.identity);
                        BombController newBombController = newBomb.GetComponent<BombController>();

                        int randomColorIndex = Random.Range(0, numColors);

                        newBombController.initialize(i, gridSizeY + 4, hexagonColors[randomColorIndex], randomColorIndex);
                        Hexagons[i, j] = newBombController;
                        Hexagons[i, j].setNewCordinates(new Vector2Int(i, j));
                        insertBomb = false;
                    }
                    else
                    {
                        GameObject newHexagon = GameObject.Instantiate(hexagon, HexagonController.calculateWorldCordinates(i, gridSizeY + 4), Quaternion.identity);
                        HexagonController newHexagonController = newHexagon.GetComponent<HexagonController>();

                        int randomColorIndex = Random.Range(0, numColors);

                        newHexagonController.initialize(i, gridSizeY + 4, hexagonColors[randomColorIndex], randomColorIndex);
                        Hexagons[i, j] = newHexagonController;
                        Hexagons[i, j].setNewCordinates(new Vector2Int(i, j));
                    }

                }
            }

        }
    }
    public static void destroyAllHexagons()
    {
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int j = 0; j < gridSizeY; j++)
            {
                Hexagons[i, j].blastHexagon();
            }
        }
    }
    public void destroyCombedHexagons()
    {
        List<HitBoxContoller> combodHitBoxes = HitBoxGridController.getCombodHitBoxes();
        foreach (HitBoxContoller currentHitBoxController in combodHitBoxes)
        {
            HexagonController[] adjustHexagons = currentHitBoxController.getAdjustHexagons();
            for (int i = 0; i < adjustHexagons.Length; i++)
            {
                try
                {
                    adjustHexagons[i].blastHexagon();
                    playerController.increaseScoreBy5();
                    PlayerController.destroyedHexagonCounter += 1;

                }
                catch
                {

                }

            }
        }
    }
    public void rotateAroundCurser(bool isRight)
    {
        curserController.isRotating = true;
        curserController.isDirectionRight = isRight;
        rotateAroundCurser120(isRight);

    }
    public void rotateAroundCurser120(bool isRight)
    {
        curserController.rotationCounter += 1;
        if (curserController.rotationCounter == 3)
        {
            curserController.rotationDone();
        }
        HexagonController[] adjustHexagons = curserController.curserHitBox.getAdjustHexagons();
        Vector2Int[] shiftedHexagons = new Vector2Int[3];
        if (isRight)
        {
            shiftedHexagons[0] = new Vector2Int(adjustHexagons[1].x, adjustHexagons[1].y);
            shiftedHexagons[1] = new Vector2Int(adjustHexagons[2].x, adjustHexagons[2].y);
            shiftedHexagons[2] = new Vector2Int(adjustHexagons[0].x, adjustHexagons[0].y);

        }
        else
        {
            shiftedHexagons[0] = new Vector2Int(adjustHexagons[2].x, adjustHexagons[2].y);
            shiftedHexagons[1] = new Vector2Int(adjustHexagons[0].x, adjustHexagons[0].y);
            shiftedHexagons[2] = new Vector2Int(adjustHexagons[1].x, adjustHexagons[1].y);

        }

        for (int i = 0; i < 3; i++)
        {
            adjustHexagons[i].setNewCordinates(shiftedHexagons[i]);
            GridController.Hexagons[shiftedHexagons[i].x, shiftedHexagons[i].y] = adjustHexagons[i];
        }
    }

}




