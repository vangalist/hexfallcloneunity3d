﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update
    public static int score = 0;
    public int highScore;
    public static int numOfMoves;
    public static int destroyedHexagonCounter=0;
    public static int destroyedBombCounter=0;
    private int oldBombScore=0;
    public int bombPeriod=100; 
    public ScoreUIController scoreUIController;
    void Awake()
    {
        readScore();
        readHighScore();
        readNumOfMoves();
        scoreUIController.initialize(score, highScore, numOfMoves);
    }
    public int readScore()
    {
        score = PlayerPrefs.GetInt("score");
        return score;
    }
    public void setScore(int newScore)
    {
        score = newScore;
        PlayerPrefs.SetInt("score", score);
        setHighScore(score);
        scoreUIController.setScoreText(score);
    }
    public void increaseScoreBy5()
    {
        setScore(score + 5);
        if (score - oldBombScore > bombPeriod)
        {
            oldBombScore = score;
            GridController.insertBomb = true;
        }
    }
    public int readHighScore()
    {
        highScore = PlayerPrefs.GetInt("highScore");
        return highScore;
    }
    public void setHighScore(int newHighScore)
    {
        if (newHighScore > highScore)
        {
            highScore = newHighScore;
            PlayerPrefs.SetInt("highScore", highScore);
            scoreUIController.setHighScoreText(highScore);
        }
    }
    public int readNumOfMoves()
    {
        numOfMoves = PlayerPrefs.GetInt("numOfMoves");
        return numOfMoves;
    }
    public void setNumOfMoves(int newNumOfMoves)
    {
        numOfMoves = newNumOfMoves;
        PlayerPrefs.SetInt("numOfMoves", numOfMoves);
        scoreUIController.setNumOfMovesText(numOfMoves);

    }
    public void increaseNumOfMovesBy1()
    {
        numOfMoves += 1;
        setNumOfMoves(numOfMoves);
    }
    public void restartGame(){
        setNumOfMoves(0);
        setScore(0);
        destroyedBombCounter=0;
        destroyedHexagonCounter=0;
        oldBombScore=50;
    }

}
