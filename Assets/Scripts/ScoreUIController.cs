﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreUIController : MonoBehaviour
{
    // Start is called before the first frame update

    public Text HighScoreText;
    public Text scoreText;
    public Text numOfMovesText;
    public void setHighScoreText(int highScore){
        HighScoreText.text= highScore.ToString();
    }
    public void setScoreText(int score){
        scoreText.text= score.ToString();
    }
    public void setNumOfMovesText(int numOfMoves){
        numOfMovesText.text= numOfMoves.ToString();
    }
    public void initialize(int score,int highScore,int numOfMoves)
    {
        setHighScoreText(highScore);
        setScoreText(score);
        setNumOfMovesText(numOfMoves);
    }


}
