﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class BombController : HexagonController
{
    public TextMeshPro bombCountDown;
    private int lifeTime = 7;
    private int numMoveOfCreation;
    private int countDown = 7;
    private Animator anima;
    private GameController gameController;
    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        anima = gameObject.GetComponent<Animator>();
        lifeTime = Random.Range(5, 7);
        setBombTimer(PlayerController.numOfMoves + 1);
        countDown = lifeTime;
    }

    void calculateCountDown(int currentMove)
    {
        countDown = numMoveOfCreation + lifeTime - currentMove;
    }
    public void setBombTimer(int newNumOfCreation)
    {
        numMoveOfCreation = newNumOfCreation;
        calculateCountDown(numMoveOfCreation);
    }
    void updateBombTimer(int newCurrentMove)
    {
        calculateCountDown(newCurrentMove);
        if (countDown == 0)
        {
            gameController.gameOver();
        }
        if (countDown < 0)
        {
            countDown = lifeTime;
        }
        bombCountDown.text = countDown.ToString();
        try
        {

            anima.Play("bombAnim");
        }
        catch
        {
            Debug.Log("Couldnt play anim");
        }
    }
    public static void updateAllBombCounters()
    {
        int numOfMoves = PlayerController.numOfMoves;
        GameObject[] allBombs = GameObject.FindGameObjectsWithTag("Bomb");
        for (int i = 0; i < allBombs.Length; i++)
        {
            try
            {

                BombController currentBomb = allBombs[i].GetComponent<BombController>();
                currentBomb.updateBombTimer(numOfMoves);
            }
            catch
            {
                Debug.Log("bomb destroyed");
            }
        }
    }
}
