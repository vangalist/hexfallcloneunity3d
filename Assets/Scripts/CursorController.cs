﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorController : MonoBehaviour
{
    // Start is called before the first frame update
    public int x;
    public int y;
    public bool isReversed;
    private int gridSizeX;
    private int gridSizeY;
    private float gridLength;

    public int rotationCounter = 0;
    public bool isRotating = false;
    public bool isDirectionRight = false;
    public HitBoxContoller curserHitBox;

    void Update()
    {

    }
    public void rotationDone()
    {

        rotationCounter = 0;
         isRotating = false;
    }
    public void initialize(int newGridSizeX, int newGridSizeY, float newGridLenght)
    {
        gridSizeX = newGridSizeX;
        gridSizeY = newGridSizeY;
        gridLength = newGridLenght;
    }
    public void setCurserTo(HitBoxContoller curHitBox)
    {
        x = curHitBox.x;
        y = curHitBox.y;
        isReversed = curHitBox.isReverse;
        curserHitBox = curHitBox;
        gameObject.SetActive(true);
        if (isReversed)
        {

            gameObject.transform.localScale = new Vector3(0.9f, 0.9f, 1);
            float calculatedX = ((float)x - (float)gridSizeX / 2 + 1) * gridLength * Mathf.Sqrt(3) / 2;
            float calculatedY = ((float)y - (float)gridSizeY / 2) * gridLength;
            if (x % 2 == 1)
            {
                calculatedY = calculatedY - 0.5f * gridLength;
            }
            gameObject.GetComponent<Transform>().position = new Vector3(calculatedX, calculatedY, 0);
        }
        else
        {
            gameObject.transform.localScale = new Vector3(-0.9f, 0.9f, 1);
            float calculatedX = ((float)x - (float)gridSizeX / 2 + 1) * gridLength * Mathf.Sqrt(3) / 2;
            float calculatedY = ((float)y - (float)gridSizeY / 2 + .5f) * gridLength;
            if (x % 2 == 1)
            {
                calculatedY = calculatedY + 0.5f * gridLength;
            }
            gameObject.GetComponent<Transform>().position = new Vector3(calculatedX, calculatedY, 0);
        }

    }

}
