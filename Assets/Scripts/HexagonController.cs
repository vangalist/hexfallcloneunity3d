﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexagonController : MonoBehaviour
{
    // Start is called before the first frame update

    public int x;
    public int y;
    public Vector2Int gridCordinates;
    public Color color;
    public int colorIndex;

    private SpriteRenderer hexagonSpriteRenderer;

    public HexagonController downHexagon;
    public HexagonController leftUpHexagon;
    public HexagonController leftDownHexagon;
    public GameObject blast;

    public float Speed = 6.5f;
    private static int gridSizeX;
    private static int gridSizeY;
    private static float gridLength;
    private Vector3 oldPosition;
    private Vector3 newPosition;
    public static int movingHexagonNumber = 0;
    bool isMoving = false;
    private GridController gridController;
    public static void setParameters(int newGridSizeX, int newGridSizeY, float newGridLenght)
    {
        gridSizeX = newGridSizeX;
        gridSizeY = newGridSizeY;

        gridLength = newGridLenght;
    }
    void Awake()
    {
        hexagonSpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        gridController = GameObject.FindGameObjectWithTag("GridController").GetComponent<GridController>();
    }
    public void blastHexagon()
    {
        GameObject blastGameObject = Instantiate(blast, transform.position, Quaternion.identity);
        ParticleSystem blastParticleSystem = blastGameObject.GetComponent<ParticleSystem>();
        blastParticleSystem.startColor = color;
        Destroy(blastGameObject, 2f);
        DestroyImmediate(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * Speed);
            Vector3 ofset = transform.position - newPosition;
            if (ofset.magnitude < 0.02f)
            {
                transform.position = newPosition;
                isMoving = false;
                movingHexagonNumber -= 1;
                if (movingHexagonNumber == 0)
                {
                    gridController.allHexagonsStopped();
                }
            }
        }

    }
    public void setColor(Color newColor, int newColorIndex)
    {
        colorIndex = newColorIndex;
        color = newColor;
        hexagonSpriteRenderer.color = color;
    }
    public void initialize(int newX, int newY, Color newColor, int newColorIndex)
    {
        setColor(newColor, newColorIndex);
        x = newX;
        y = newY;
        gridCordinates = new Vector2Int(x, y);
    }

    public static Vector2 calculateWorldCordinates(int x, int y)
    {
        float calculatedX = ((float)x - (float)gridSizeX / 2 + 0.5f) * gridLength * Mathf.Sqrt(3) / 2;
        float calculatedY = ((float)y - (float)gridSizeY / 2) * gridLength;
        if (x % 2 == 1)
        {
            calculatedY = calculatedY + 0.5f * gridLength;
        }
        return new Vector2(calculatedX, calculatedY);
    }
    public void setNewCordinates(Vector2Int newCordinates)
    {
        isMoving = true;
        movingHexagonNumber += 1;
        x = newCordinates.x;
        y = newCordinates.y;
        gridCordinates = newCordinates;
        Vector2 newCordinatesWorld = calculateWorldCordinates(x, y);
        oldPosition = transform.position;
        newPosition = new Vector3(newCordinatesWorld.x, newCordinatesWorld.y, 0);
    }
    public Vector2Int findCordinatesAfterCombo()
    {
        int cursorIndex = 0;
        while (cursorIndex < y)
        {
            if (GridController.Hexagons[x, cursorIndex] == null)
            {

                return new Vector2Int(x, cursorIndex);

            }

            else
            {
                cursorIndex = cursorIndex + 1;
            }
        }
        return gridCordinates;

    }

    private void setDownHexagon()
    {
        try
        {
            downHexagon = GridController.Hexagons[x, y - 1];
        }
        catch
        {
            downHexagon = null;
        }
    }
    private void setLeftHexagons()
    {
        if (x % 2 == 0)
        {
            try
            {
                leftDownHexagon = GridController.Hexagons[x - 1, y - 1];
            }
            catch
            {
                leftDownHexagon = null;
            }
            try
            {
                leftUpHexagon = GridController.Hexagons[x - 1, y];
            }
            catch
            {
                leftUpHexagon = null;
            }
        }
        else
        {
            try
            {
                leftDownHexagon = GridController.Hexagons[x - 1, y];
            }
            catch
            {
                leftDownHexagon = null;
            }
            try
            {
                leftUpHexagon = GridController.Hexagons[x - 1, y + 1];
            }
            catch
            {
                leftUpHexagon = null;
            }
        }

    }
    public void setAdjustHexagons()
    {
        setDownHexagon();
        setLeftHexagons();
    }
    public int forbidenColorIndex()
    {
        try
        {

            if (downHexagon.colorIndex == leftDownHexagon.colorIndex || leftDownHexagon.colorIndex == leftUpHexagon.colorIndex)
            {
                return leftDownHexagon.colorIndex;
            }
            else
            {
                return -1;
            }

        }
        catch
        {
            return -1;
        }

    }


}
