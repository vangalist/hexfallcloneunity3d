﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxGridController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject hexagonHitBox;
    private static int gridSizeX;
    private static int gridSizeY;
    private float gridLength;
    public static HitBoxContoller[,] hitBoxesNormal;
    public static HitBoxContoller[,] hitBoxesReversed;
    public void createGrid(int newGridSizeX, int newGridSizeY, float newGridLenght)
    {
        gridSizeX = newGridSizeX;
        gridSizeY = newGridSizeY;
        gridLength = newGridLenght;
        hitBoxesNormal = new HitBoxContoller[gridSizeX, gridSizeY];
        hitBoxesReversed = new HitBoxContoller[gridSizeX, gridSizeY];
        for (int i = 0; i < gridSizeX - 1; i++)

        {
            for (int j = 0; j < gridSizeY - 1; j++)
            {
                createHexagonHitBox(i, j);
            }
        }
    }
    private void createHexagonHitBox(int x, int y)
    {
        float calculatedX = ((float)x - (float)(gridSizeX - 1) / 2 + 0.5f) * gridLength * Mathf.Sqrt(3) / 2;
        float calculatedY = ((float)y - (float)(gridSizeY - 1) / 2) * gridLength;
        if (x % 2 == 1)
        {
            calculatedY = calculatedY + 0.5f * gridLength;
        }
        GameObject newHexagonHitBox = GameObject.Instantiate(hexagonHitBox, new Vector2(calculatedX, calculatedY), Quaternion.identity);
        HitBoxContoller newHitBoxController = newHexagonHitBox.GetComponent<HitBoxContoller>();
        newHitBoxController.initialize(x, y, false);
        hitBoxesNormal[x, y] = newHitBoxController;
        GameObject newHexagonHitBoxReverse = GameObject.Instantiate(hexagonHitBox, new Vector2(calculatedX, -calculatedY - 0.5f * gridLength), Quaternion.identity);
        HitBoxContoller newHitBoxControllerReverse = newHexagonHitBoxReverse.GetComponent<HitBoxContoller>();
        newHitBoxControllerReverse.initialize(x, gridSizeY - y - 1, true);
        hitBoxesReversed[x, y] = newHitBoxControllerReverse;

    }
    public static List<HitBoxContoller> getCombodHitBoxes()
    {
        List<HitBoxContoller> combodHitBoxes = new List<HitBoxContoller>();

        for (int i = 0; i < gridSizeX - 1; i++)

        {
            for (int j = 0; j < gridSizeY - 1; j++)
            {
                try
                {

                    if (hitBoxesNormal[i, j].isAdjustHexagonsCombo())
                    {
                        combodHitBoxes.Add(hitBoxesNormal[i, j]);
                    }
                    if (hitBoxesReversed[i, j].isAdjustHexagonsCombo())
                    {
                        combodHitBoxes.Add(hitBoxesReversed[i, j]);
                    }


                }
                catch
                {

                }

            }
        }


        return combodHitBoxes;
    }


}
